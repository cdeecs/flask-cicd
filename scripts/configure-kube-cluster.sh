kubectl create namespace gitlab-namespace
kubectl config set-context $(kubectl config current-context) --namespace=gitlab-namespace
kubectl create -f service-account.yml
kubectl create clusterrolebinding gitlab-cluster-admin-binding --clusterrole=cluster-admin --serviceaccount=gitlab-namespace:gitlab-user
kubectl create clusterrolebinding permissive-binding --clusterrole=cluster-admin --user=admin --user=kubelet --group=system:serviceaccounts
kubectl describe serviceAccounts gitlab-user
