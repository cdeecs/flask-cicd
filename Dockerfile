FROM python:3.5-slim
RUN pip3 install Flask gunicorn pytest
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
EXPOSE 8080
WORKDIR /app
COPY . /app
