# flask-cicd

En este repositorio vamos a crear una aplicacion de python utilizando flask y a crear un `pipeline` con pruebas y `deployment`a un cluster de kubernetes.

## SDLC

El SDLC o ciclo e vida de desarrollo de software es una metodología de varias etapas para mantener la calidad y consistencia en el desarrollo de software independientemente de que metodología de organización del trabajo se utilice. 

* Planeación
* Análisis
* Diseño
* Implementación
* Pruebas e Integración
* Puesta en Marcha (deployment)
* Mantenimiento

## Proyecto

La aplicación que se va a desarrollar es una API desarrollada en Flask que funcione como una calculadora que interprete expresiones matematicas en texto.


1. Por ejemplo: Al realiar una llamda `POST`al punto `/calcular`con el siguiente cuerpo `2+3`debemos de recibir la respuesta `5`
1. La aplicación debe de solo ser puesta en marcha en producción si cumple con las pruebas.
1. Hay que desarrollar una serie de pruebas.
1. Al pasar las pruebas se creara una imágen de docker con la aplicación.
1. El ambiente de producción será un clúster de kubernetes.
1. Para CI/CD se utilizaran los servicios disponibles de gitlab.